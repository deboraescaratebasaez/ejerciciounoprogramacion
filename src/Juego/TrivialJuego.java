package Juego;

import Users.Partida;
import ficheros.GestionaFicheros;
import preguntas.Opcion;
import preguntas.Pregunta;
import Users.Player;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import static preguntas.Pregunta.opciones;

public class TrivialJuego {
    public ArrayList<Pregunta> preguntas;
    public Users.Player player;
    public Users.Partida partida;


    public TrivialJuego(Player player) {
        this.preguntas = preguntas;
        this.player = player;
        this.partida = partida;
    }

    public void jugar(){
        ArrayList<Pregunta> preguntas = GestionaFicheros.cargaPreguntas();
        Collections.shuffle(preguntas);
        Partida partida = new Partida(player);
        Scanner scanner = new Scanner(System.in);
        int numPreguntasMostradas = 0;
        final int Num_Preguntas = 5;

        System.out.println("¡Bienvenido al Trivial, " + player.nombre + "!");
        System.out.println("Responde a las siguientes preguntas:");



        for (Pregunta pregunta : preguntas) {
            if (numPreguntasMostradas == Num_Preguntas) {
               break;
            } else {
                System.out.println(pregunta.getPregunta());
                Opcion[] opciones = obtenerOpcionesAleatorias(pregunta.opciones);
                mostrarOpciones(opciones);

                System.out.print("Elige una opción: ");
                String respuesta = scanner.nextLine();

                if (respuesta.equalsIgnoreCase("A")) {
                    System.out.println("¡Respuesta correcta!");
                    partida.sumarPunto();
                } else {
                    System.out.println("Respuesta incorrecta. La respuesta correcta es: " + pregunta.getOpcionCorrecta());
                }

                numPreguntasMostradas++;
                System.out.println();
            }
        }

        System.out.println("Has terminado el juego. Tu puntuación final es: " + partida.puntuacion);
        GestionaFicheros.guardaPartida(partida);
    }


    private Opcion[] obtenerOpcionesAleatorias(Opcion[] opciones) {
        //Collections.shuffle(List.of(opciones));
        return opciones;
    }

    private void mostrarOpciones(Opcion[] opciones) {
        char letra = 'A';
        for (Opcion opcion : opciones) {
            System.out.println(letra + ") " + opcion.getEnunciado());
            letra++;
        }
    }


}
