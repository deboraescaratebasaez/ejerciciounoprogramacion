package Juego;

import java.util.ArrayList;
import java.util.Scanner;

import Users.Player;
import ficheros.GestionaFicheros;
import Users.User;

public class TrivialMain {

    private static ArrayList<User> users;
    public static void main(String[] args) {
        GestionaFicheros gestionaFicheros = new GestionaFicheros();
        users = gestionaFicheros.cargaUsers();
        Scanner numero = new Scanner(System.in);
        Scanner letra = new Scanner(System.in);
        int opcion;
        do {
            menu();
            opcion = numero.nextInt();

            switch (opcion) {
                case 1:
                    registroPlayer(letra);
                    break;
                case 2:
                    registroAdmin(letra);
                    break;
                case 3:
                    inicioSesion(letra);
                    break;
                case 4:
                    System.out.println("Saliendo...");
                    break;
                default:
                    System.out.println("Opción inválida. Inténtalo de nuevo.");
            }
        } while (opcion != 4);

    }


    public static void menu() {
        System.out.println("_______________________________ \n" +
                "|     1. Registro player       | \n" +
                "|     2. Registro admin        | \n" +
                "|     3. Inicio de sesión      | \n" +
                "|     4. Salir                 | \n" +
                "|______________________________|");
    }

    public static void registroPlayer(Scanner letra) {
        System.out.println("Escribe un nombre: ");
        String username = letra.nextLine();
        System.out.println("Escribe una contraseña(mínimo 8 caracteres):");
        String cont1 = letra.nextLine();
        System.out.println("Vuelve a escribir la contraseña:");
        String cont2 = letra.nextLine();
        if (cont1.equals(cont2) && cont1.length() >= 8) {
            Users.User usuario = new User(username, cont1) {
                @Override
                public boolean permisosAdmin() {
                    return false;
                }
            };
            users.add(usuario);
            GestionaFicheros.guardaUser(users);

            System.out.println("Usuario registrado exitosamente.");
        } else {
            System.out.println("Las contraseñas no coinciden o no cumplen con los requisitos.");
        }
    }

    public static void registroAdmin(Scanner letra) {
        System.out.println("Escribe un nombre: ");
        String username = letra.nextLine();
        System.out.println("Escribe una contraseña(mínimo 8 caracteres):");
        String cont1 = letra.nextLine();
        System.out.println("Vuelve a escribir la contraseña:");
        String cont2 = letra.nextLine();
        if (cont1.equals(cont2) && cont1.length() >= 8) {
            Users.User usuario = new User(username, cont1) {
                @Override
                public boolean permisosAdmin() {
                    return true;
                }
            };
            users.add(usuario);
            GestionaFicheros.guardaUser(users);
            System.out.println("Administrador registrado exitosamente.");
        } else {
            System.out.println("Las contraseñas no coinciden o no cumplen con los requisitos.");
        }
    }

    public static void inicioSesion(Scanner letra) {
        System.out.println("Nombre de usuario: ");
        String username = letra.nextLine();
        System.out.println("Contraseña: ");
        String cont1 = letra.nextLine();
        boolean usuarioEncontrado = false;
        for (User user : users) {
            if (user.nombre.equals(username) && user.compruebaPass(cont1)) {
                usuarioEncontrado = true;
                if (user.permisosAdmin()) {
                    System.out.println("Iniciaste sesión como administrador.");
                    TrivialAdmin.administrar();
                } else if (!user.permisosAdmin()) {
                    System.out.println("Iniciaste sesión como jugador.");
                    Player jugador = new Player(username, cont1);
                    TrivialJuego juego = new TrivialJuego(jugador);
                    juego.jugar();
                }
                break;
            }
        }

        if (!usuarioEncontrado) {
            System.out.println("Nombre de usuario o contraseña incorrectos.");
        }

    }
}
