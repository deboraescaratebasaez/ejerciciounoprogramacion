package Juego;
import ficheros.GestionaFicheros;
import Users.User;
import Users.Partida;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TrivialAdmin {
    private ArrayList<Partida> partidas;
    private ArrayList<User> users;
    public TrivialAdmin(ArrayList<Partida> partidas, ArrayList<User> users) {
        this.partidas = partidas;
        this.users = users;
    }

    public static void administrar() {

        // Mostrar todas las partidas
        ArrayList<String> partidas = GestionaFicheros.leePartidas();
        if (partidas.isEmpty()) {
            System.out.println("No hay partidas registradas.");
        } else {
            System.out.println("Partidas registradas:");
            for (String partida : partidas) {
                System.out.println(partida);
            }
        }

        // Mostrar todos los nombres de usuarios ordenados alfabéticamente
        List<User> users = GestionaFicheros.cargaUsers();
        if (users.isEmpty()) {
            System.out.println("No hay usuarios registrados.");
        } else {
            System.out.println("Usuarios registrados:");
            for (User user : users) {
                System.out.println(user.nombre);
            }
        }
    }
}
