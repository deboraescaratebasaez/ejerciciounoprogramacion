package preguntas;

import java.util.ArrayList;

public class Pregunta {
    public String pregunta;
    public  Opcion[] opciones;

    public Pregunta(String pregunta, preguntas.Opcion[] opciones) {
        this.pregunta = pregunta;
        this.opciones = opciones;
    }

    public static ArrayList<String> opciones() {
        ArrayList<String> opciones = new ArrayList<>();
        return opciones;

    }

    public String getPregunta() {
        return pregunta;
    }

    public static Opcion[] getOpciones() {
        return getOpciones();
    }

    public String getOpcionCorrecta() {
        for (Opcion opcion : opciones) {
            if (opcion.esCorrecta()) {
                return opcion.getEnunciado();
            }
        }
        return null;
    }

    public boolean esCorrecta(int indice) {
        return opciones[indice].esCorrecta();
    }
}
