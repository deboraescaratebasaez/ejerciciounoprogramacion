package Users;
import java.util.Date;

public class Partida {

    // atributos
    public Date date;
    public int puntuacion;
    public Player player;

    // constructor
    public Partida(Player player) {
        this.date = new Date();
        this.puntuacion = 0;
        this.player = player;
    }

    // metodos
    public int sumarPunto(){
        return this.puntuacion++;
    }

}
