package Users;

public abstract class User {
    // atributos
    public String nombre;
    public String pass;


    // constructor
    public User(String nombre, String pass) {
        this.nombre = nombre;
        this.pass = pass;
    }


    // metodos
    public boolean cambiarPass(String pass){

        if(pass.length() >= 8){
            System.out.println("Se ha cambiado la contraseña exitosamente.");
            this.pass = pass;
            return true;
        }else{
            System.out.println("Error: Fallo al cambiar la contraseña, la contraseña no cumple con los parámetros establecidos.");
            System.out.println("(La contraseña ha de tener más de 8 dígitos).");
            return false;
        }
    }

    public boolean compruebaPass(String pass){
        return this.pass.equals(pass);
    }

    public abstract boolean permisosAdmin();

}
