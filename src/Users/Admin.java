package Users;

public class Admin extends User{

    public Admin(String nombre, String pass) {
        super(nombre, pass);
    }

    @Override
    public boolean permisosAdmin() {
        System.out.println("El usuario posee permisos de administrador.");
        return true;
    }
}
