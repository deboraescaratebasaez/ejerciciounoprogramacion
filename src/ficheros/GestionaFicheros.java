package ficheros;

import Users.User;
import preguntas.Opcion;
import preguntas.Pregunta;
import Users.Partida;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.io.*;
import java.util.ArrayList;

public class GestionaFicheros {
    private static final String filePreguntas = "src\\ficheros\\preguntas.txt";
    private static final String fileUser = "src\\ficheros\\user2.bin";
    private static final String partidas ="src\\ficheros\\partidas.txt";




    public static void guardaUser2 (ArrayList<User> users){
        // while ()
        try {
            FileWriter fw = new FileWriter(fileUser);
            BufferedWriter bw = new BufferedWriter(fw);
            //Recorrer el arrylist del User y escribir cada usuario en el fichero de usuariios
            for (int i = 0; i <users.size() ; i++) {
                bw.write(users.get(i).nombre + "," + users.get(i).pass);
                bw.close();

                /*(ObjectOutputStream oos = new ObjectOutputStream(new File("users.dat"))) {
        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            oos.writeObject(user);*/
            }
            //nose si cada vez que se llama al metodo  se sobreescribe

        } catch (IOException e) {
            System.out.println("Error de IO");
        }
    }
    public static void guardaUser(ArrayList<User> users) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileUser))) {
            for (User user : users) {
                bw.write(user.nombre + "," + user.pass);
                bw.newLine(); // Agregar una nueva línea después de cada usuario
            }
        } catch (IOException e) {
            e.printStackTrace();
            // Manejar la excepción de manera adecuada, como mostrar un mensaje de error al usuario
            // o registrar el error en un archivo de registro
        }
    }


    public static ArrayList<User> cargaUsers() {
        ArrayList<User> users = new ArrayList<>();

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileUser))) {
            while (true) {
                User user = (User) ois.readObject();
                users.add(user);
            }
        } catch (EOFException e) {
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return users;
    }

    public static ArrayList<Pregunta> cargaPreguntas() {
        ArrayList<Pregunta> preguntas = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(filePreguntas))) {
            String pregunta;
            while ((pregunta = br.readLine()) != null) {
                // Leer la respuesta correcta y las tres respuestas incorrectas
                String respuestaCorrecta = br.readLine();
                String[] respuestasIncorrectas = new String[3];
                for (int i = 0; i < 3; i++) {
                    respuestasIncorrectas[i] = br.readLine();
                }

                // Crear las opciones
                Opcion[] opciones = new Opcion[4];
                opciones[0] = new Opcion(respuestaCorrecta, true);
                for (int i = 0; i < 3; i++) {
                    opciones[i + 1] = new Opcion(respuestasIncorrectas[i], false);
                }

                // Crear la pregunta y agregarla a la lista
                preguntas.add(new Pregunta(pregunta, opciones));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return preguntas;
    }

    public static ArrayList<String> guardaPartida (Partida partida) {
       // if (partidas.exists()) {
            try {
                FileWriter fw = new FileWriter(partidas, true);
                BufferedWriter bw1 = new BufferedWriter(fw);

                bw1.write(partida.date.toString() + partida.date.getHours() + partida.player + partida.puntuacion);
                bw1.close();

            } catch (IOException e) {
                System.out.println("Error de IO");
            }
       // }
        return null;
    }

    public static ArrayList<String> leePartidas(){
        ArrayList<String> partidas = new ArrayList<>();
        try(BufferedReader br1 = new BufferedReader(new FileReader(String.valueOf(partidas)))){
            String line;
            while ((line = br1.readLine()) != null){
                partidas.add(line);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        return partidas;
    }

}

